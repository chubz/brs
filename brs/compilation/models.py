from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill, SmartResize


def upload_to_path(instance, filename):
    return 'images/{}/{}'.format(instance.author.username if instance.author else 'user_anon', filename)


class CompilationVolume(models.Model):
    volume = models.SmallIntegerField(_(u'volume'))
    title = models.CharField(_(u'title'), max_length=50, blank=True)
    slug = models.SlugField(_(u'slug'), max_length=50, unique=True)
    pub_date = models.DateTimeField(_(u'publication date'))
    description = models.TextField(_(u'description'))
    info = models.TextField(_(u'extended information'), blank=True)
    cover = models.ImageField(_(u'cover'), upload_to=upload_to_path, max_length=255)
    # cover_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='cover', format='JPEG', options={'quality': 90})
    songs = models.ManyToManyField('Song', verbose_name=_(u'songs'))
    # gallery = models.ForeignKey(_(u'gallery'), null=True, blank=True)

    class Meta:
        verbose_name = _('Compilation Volume')
        verbose_name_plural = _('Compilation Volumes')

    def __unicode__(self):
        return u'Bistro vol{}'.format(self.volume)


class Song(models.Model):
    title = models.CharField(_(u'title'), max_length=50)
    slug = models.SlugField(_(u'slug'), max_length=50, unique=True)
    artist = models.ForeignKey('Artist', verbose_name=_(u'artist'), related_name='artist')
    featuring = models.ManyToManyField('Artist', verbose_name=_(u'featuring'), null=True, blank=True, related_name='featuring')
    album = models.ForeignKey('Album', verbose_name=_(u'album'))
    info = models.TextField(_(u'extended information'), blank=True)
    genre = models.ManyToManyField('Genre', verbose_name=_(u'genre'))
    lyrics = models.TextField(_(u'lyrics'), blank=True)
    pub_date = models.DateTimeField(_(u'publication date'))
    song_mp3 = models.FileField(_(u'song mp3'), upload_to=upload_to_path, max_length=255)
    song_ogg = models.FileField(_(u'song oga'), upload_to=upload_to_path, max_length=255)

    class Meta:
        verbose_name = _('Song')
        verbose_name_plural = _('Songs')

    def __unicode__(self):
        return u'{} - {}'.format(self.album, self.title)


class Album(models.Model):
    artist = models.ForeignKey('Artist', verbose_name=_(u'artist'))
    title = models.CharField(_(u'title'), max_length=50)
    slug = models.SlugField(_(u'slug'), max_length=50, unique=True)
    pub_date = models.DateTimeField(_(u'publication date'))
    cover = models.ImageField(_(u'cover'), upload_to=upload_to_path, max_length=255, null=True, blank=True)
    # cover_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='cover', format='JPEG', options={'quality': 90})
    description = models.TextField(_(u'description'))
    info = models.TextField(_(u'extended information'), blank=True)
    genre = models.ManyToManyField('Genre', verbose_name=_(u'genre'))
    songs = models.ManyToManyField('Song', verbose_name=_(u'songs'), related_name='album_songs')

    class Meta:
        verbose_name = _('Album')
        verbose_name_plural = _('Albums')

    def __unicode__(self):
        return u'{} - {}'.format(self.artist.name, self.title)


class Artist(models.Model):
    name = models.CharField(_(u'name'), max_length=50)
    slug = models.SlugField(_(u'slug'), max_length=50, unique=True)
    email = models.EmailField(_(u'e-mail'), blank=True)
    web_site = models.URLField(_(u'web site'), blank=True)
    band_picture = models.ImageField(_(u'band picture'), upload_to=upload_to_path, max_length=255, blank=True)
    # band_picture_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='band_picture', format='JPEG', options={'quality': 90})
    description = models.TextField(_(u'description'))
    # gallery = models.ForeignKey(_(u'gallery'), blank=True)

    class Meta:
        verbose_name = _('Artist')
        verbose_name_plural = _('Artists')

    def __unicode__(self):
        return self.name


class Genre(MPTTModel):
    # should be an mptt model defining things like rock>post rock > surf rock etc ...
    name = models.CharField(_(u'name'), max_length=50)
    slug = models.SlugField(_(u'slug'), max_length=50, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['category_name']

    class Meta:
        verbose_name = _('Genre')
        verbose_name_plural = _('Genres')

    def __unicode__(self):
        return self.name
