from __future__ import absolute_import
from django.views.generic import DetailView, TemplateView
from .models import CompilationVolume, Artist, Album, Song
# from django.shortcuts import get_object_or_404
# from djpjax import PJAXResponseMixin
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from django.core.urlresolvers import reverse_lazy


class HomeView(TemplateView):
    template_name = "index.html"

index = HomeView.as_view()
'''
class HomeView(DetailView):
    model = CompilationVolume
    template_name = 'index.html'


index = HomeView.as_view()
'''


class SongDetail(DetailView):
    model = Song


class AlbumDetail(DetailView):
    model = Album


class ArtistDetail(DetailView):
    model = Artist
