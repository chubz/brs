from django.db import models
from django.utils.translation import ugettext_lazy as _
# from imagekit.models import ImageSpecField
# from imagekit.processors import ResizeToFill, SmartResize


def upload_to_path(instance, filename):
    return 'images/{}/{}'.format(instance.author.username if instance.author else 'user_anon', filename)


class Blog(models.Model):
    title = models.CharField(_(u'title'), max_length=66)
    slug = models.SlugField(_(u'slug'), unique=True, max_length=66)
    category = models.ForeignKey('Category', verbose_name=_(u'category'))
    pub_date = models.DateTimeField()
    text = models.TextField(_(u'text'))

    class Meta:
        verbose_name = _('Blog')
        verbose_name_plural = _('Blogs')

    def __unicode__(self):
        return self.title


class Category(models.Model):
    name = models.CharField(_(u'name'), max_length=50)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __unicode__(self):
        return self.name


class Gallery(models.Model):
    name = models.CharField(_(u'name'), max_length=50)
    slug = models.SlugField(_(u'slug'), unique=True, max_length=66)
    timeframe = models.DateTimeField(_(u'timeframe'), blank=True)
    pub_date = models.DateTimeField()
    cover = models.ImageField(_(u'cover'), upload_to=upload_to_path, max_length=255)
    # cover_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='cover', format='JPEG', options={'quality': 90})
    description = models.TextField(_(u'description'))
    photos = models.ManyToManyField('Photos', verbose_name=_(u'name'))

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')

    def __unicode__(self):
        return self.name


class Photos(models.Model):
    title = models.CharField(_(u'title'), max_length=50)
    slug = models.SlugField(_(u'slug'), unique=True, max_length=66)
    description = models.TextField(_(u'description'), blank=True)
    pub_date = models.DateTimeField()
    photo = models.ImageField(_(u'photo'), upload_to=upload_to_path, max_length=255)
    # photo_thumb = ImageSpecField((ResizeToFill(75, 75), ), image_field='photo', format='JPEG', options={'quality': 90})

    class Meta:
        verbose_name = _('Photos')
        verbose_name_plural = _('Photoss')

    def __unicode__(self):
        return self.title



