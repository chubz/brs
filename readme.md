Bistro na rubu šume
====================

Bistro na rubu šume je kompilacija autorskih pjesama bendova i kantautora sa balkanskih prostora. Da bi što bolje prezentirali samu kompilaciju i njene autore odlučili smo izraditi ovu stranicu. Ovaj projekt će nam omogućiti da osim ustaljenih distribucijskih kanala stvorimo mjesto na kojem se ljudi mogu naći i snaći te možda dobiti i dodatne infomracije o samim izvođačima koje primjerice putem drugih servisa ne bi mogli dobiti.

Slika osnovne ideje layouta dostupna na:
http://imageshack.us/photo/my-images/600/yyrp.png/
http://img600.imageshack.us/img600/2816/yyrp.png